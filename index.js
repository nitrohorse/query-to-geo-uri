let timer = 0

const delayedGetSuggestions = query => {
  if (query === '') {
    document.getElementById('loadingSpinner').style.visibility = 'hidden'
    deleteSuggestions()
  } else {
    document.getElementById('loadingSpinner').style.visibility = 'visible'
  }
  if (timer) {
    window.clearTimeout(timer)
  }
  timer = window.setTimeout(() => {
    getSuggestions(query)
  }, 500)
}

const deleteSuggestions = () => {
  const suggestions = document.getElementById('suggestions')
  while (suggestions.firstChild) {
    suggestions.removeChild(suggestions.firstChild)
  }
}

const displaySuggestions = (suggestions, status) => {
  if (status !== google.maps.places.PlacesServiceStatus.OK) {
    return
  }
  deleteSuggestions()
  suggestions.forEach(suggestion => {
    service = new google.maps.places.PlacesService(document.createElement('div'))
    service.getDetails({ placeId: suggestion.place_id }, addPlaceDetailsToView)
  })
}

const getSuggestions = input => {
  if (input === '') {
    return
  }
  const service = new google.maps.places.AutocompleteService()
  service.getPlacePredictions({ input: input }, displaySuggestions)
}

const addPlaceDetailsToView = (place, status) => {
  document.getElementById('loadingSpinner').style.visibility = 'hidden'
  if (status !== google.maps.places.PlacesServiceStatus.OK) {
    return
  }
  const lat = place.geometry.location.lat()
  const lng = place.geometry.location.lng()
  const latLng = lat + ',' + lng
  const placeDetails = '  ' + place.name + ' | ' + place.formatted_address + ' | ' + latLng
  const button = document.createElement('input')
  button.type = 'button'
  button.className = 'pure-button pure-button-primary button-xsmall'
  button.value = 'Go'
  const geoUri = 'geo:' + latLng
  button.onclick = () => {
    window.location = geoUri
    return false
  }
  const li = document.createElement('li')
  li.appendChild(button)
  li.appendChild(document.createTextNode(placeDetails))
  document.getElementById('suggestions').appendChild(li)
}
