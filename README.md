# Query to Geo URI

[![License](https://img.shields.io/badge/license-GPLv3-yellow.svg)](https://github.com/nitrohorse/query-to-geo-uri/blob/master/LICENSE)
[![Say Thanks!](https://img.shields.io/badge/Say%20Thanks-!-1EAEDB.svg)](https://saythanks.io/to/nitrohorse)

Inspired by the open-source Android app, [AddressToGPS](https://github.com/DanielBarnett714/AddressToGPS), [found on F-Droid](https://f-droid.org/en/packages/me.danielbarnett.addresstogps/).


When given a location query, this app queries Google's [Maps JavaScript API](https://developers.google.com/maps/documentation/javascript/places) for place predictions and displays a hyperlinked [geo URI](https://en.wikipedia.org/wiki/Geo_URI_scheme) button with each place's longitude and latitude which mobile browsers can then parse as locations. This is useful for those who use an offline, [OpenStreetMap](https://en.wikipedia.org/wiki/OpenStreetMap) data app like [OsmAnd](https://osmand.net/) or [MAPS.ME](https://maps.me/) on their mobile device.